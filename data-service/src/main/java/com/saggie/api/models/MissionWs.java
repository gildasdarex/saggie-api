/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/18/18
 */
package com.saggie.api.models;

import com.saggie.commons.ws.enums.SaggieMissionStatusEnums;
import com.saggie.commons.ws.utils.DateUtils;
import com.saggie.commons.ws.utils.SaggieObjectUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MissionWs {

    @Mapping("id")
    private Integer id;
    private String agentUsername;
    @Mapping("name")
    private String name;
    @Enumerated(EnumType.STRING)
    private SaggieMissionStatusEnums status;
    private String startAt;
    private String endAt;

    public Date getStartDateAsDateType() throws ParseException {
        if (SaggieObjectUtils.isNotNull(startAt))
            return DateUtils.parse(startAt);
        else
            return null;
    }

    public Date getEndDateAsDateType() throws ParseException {
        if (SaggieObjectUtils.isNotNull(endAt))
            return DateUtils.parse(endAt);
        else
            return null;
    }

    public Timestamp getStartDateAsTimestampType() throws ParseException {

        if (SaggieObjectUtils.isNotNull(startAt)) {
            Date date = DateUtils.parse(startAt);
            return new Timestamp(date.getTime());
        }
        else
            return null;
    }

    public Timestamp getEndDateAsTimestampType() throws ParseException {
        if (SaggieObjectUtils.isNotNull(endAt)) {
            Date date = DateUtils.parse(endAt);
            return new Timestamp(date.getTime());
        }
        else
            return null;
    }

}
