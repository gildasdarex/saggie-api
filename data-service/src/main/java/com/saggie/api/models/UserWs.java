/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/18/18
 */
package com.saggie.api.models;


import com.saggie.commons.ws.enums.SaggieRoleEnums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserWs {

    @Mapping("id")
    private Integer id;
    @Mapping("lastName")
    private String lastName;
    @Mapping("username")
    @Email(message = "must be an email")
    @NotEmpty(message = "must not be empty")
    private String username;
    @Enumerated(EnumType.STRING)
    @NotNull
    private SaggieRoleEnums roleName ;
    private String userStatus;
    @Mapping("firstName")
    private String firstName;
    @NotBlank
    private String password;


}
