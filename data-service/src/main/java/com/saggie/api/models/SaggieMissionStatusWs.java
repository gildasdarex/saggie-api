/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/18/18
 */
package com.saggie.api.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.dozer.Mapping;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaggieMissionStatusWs {

    @Mapping("id")
    private Integer id;
    @Mapping("name")
    private String name;
    @Mapping("description")
    private String description;


}
