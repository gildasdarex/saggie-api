package com.saggie.api.configs;

import io.micrometer.spring.autoconfigure.MeterRegistryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MicrometerConfiguration {

    @Bean
    MeterRegistryCustomizer meterRegistryCustomizer() {
        return registry -> registry.config()
                .commonTags("service", "saggie-api");
    }

}
