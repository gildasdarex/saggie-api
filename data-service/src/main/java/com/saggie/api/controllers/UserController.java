package com.saggie.api.controllers;


import com.saggie.api.models.UserWs;
import com.saggie.api.services.UserService;
import com.saggie.api.utils.ApiRequestUtils;
import com.saggie.api.version.ApiVersions;
import com.saggie.commons.ws.exceptions.SaggieApiException;
import com.saggie.commons.ws.logger.SaggieLogger;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.models.SaggieRequestContext;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping("/users")
public class UserController {
    private static Logger logger = LogManager.getLogger(UserController.class);
    @Autowired
    HttpServletRequest httpServletRequest;
    @Autowired
    private UserService userService;

    @PostMapping("/**")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "Create agent/leader", notes = "Create agent/leader. Only Admin can create mission")
    public ResponseEntity<ApiCommonResponseWs> create(@AuthenticationPrincipal Authentication principal,
                                                      @RequestBody @Valid UserWs userWs) throws SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = userService.create(saggieRequestContext, userWs);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }

    @GetMapping("/**")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")

    @ApiOperation(value = "List Agents/leaders", notes = "List Agents/leader. Only Admin can view users")
    public ResponseEntity<ApiCommonResponseWs> list(@AuthenticationPrincipal Authentication principal){
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = userService.list(saggieRequestContext);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }

    @GetMapping("/{userId}")
    @ApiVersions({"1"})
    @ApiOperation(value = "user info by id", notes = "See uer info by id. All can view his/her info")
    public ResponseEntity<ApiCommonResponseWs> getById(@AuthenticationPrincipal Authentication principal,
                                                       @PathVariable(name = "userId") Integer userId) throws SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = userService.getById(saggieRequestContext, userId);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }

    @DeleteMapping("/{userId}")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "Delete user", notes = "Delete a user. Only Admin can delete user")
    public ResponseEntity<?> delete(@AuthenticationPrincipal Authentication principal, @PathVariable Integer userId) throws SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = userService.delete(saggieRequestContext, userId);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }

    @PutMapping("/{id}")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    @ApiOperation(value = "Update User", notes = "Update a User. Only Admin can update mission")
    public ResponseEntity<ApiCommonResponseWs> update(@AuthenticationPrincipal Authentication principal,
                                                      @RequestBody @Valid UserWs userWs, @PathVariable Integer id) throws SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = userService.update(saggieRequestContext, userWs, id);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }

}