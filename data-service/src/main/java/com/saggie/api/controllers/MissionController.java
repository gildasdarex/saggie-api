package com.saggie.api.controllers;

import com.saggie.api.models.MissionWs;
import com.saggie.api.services.MissionService;
import com.saggie.api.utils.ApiRequestUtils;
import com.saggie.api.version.ApiVersions;
import com.saggie.commons.ws.exceptions.SaggieApiException;
import com.saggie.commons.ws.logger.SaggieLogger;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.models.SaggieRequestContext;
import io.swagger.annotations.ApiOperation;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;

@RestController
@RequestMapping("/missions")
public class MissionController {

    private static Logger logger = LogManager.getLogger(UserController.class);
    @Autowired
    HttpServletRequest httpServletRequest;
    @Autowired
    private MissionService missionService;

    @PostMapping("/**")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_LEADER')")
    @ApiOperation(value = "Create Mission", notes = "Create Mission. Only Leader can create mission")
    public ResponseEntity<ApiCommonResponseWs> create(@AuthenticationPrincipal Authentication principal,
                                                      @RequestBody @Valid MissionWs missionWs) throws ParseException, SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = missionService.create(saggieRequestContext, missionWs);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }


    @PutMapping("/**")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_LEADER')")
    @ApiOperation(value = "Update Mission", notes = "Update a Mission. Only Leader can update mission")
    public ResponseEntity<ApiCommonResponseWs> update(@AuthenticationPrincipal Authentication principal,
                                                      @RequestBody @Valid MissionWs missionWs) throws ParseException, SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = missionService.update(saggieRequestContext, missionWs);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }


    @GetMapping("/**")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_LEADER')")
    @ApiOperation(value = "List all Missions", notes = "Lists all Missions by agent or not. Agent & Leader can list all missions")
    public ResponseEntity<ApiCommonResponseWs> listMissionsByAgent(@AuthenticationPrincipal Authentication principal,
                                                                   @RequestParam(name = "agent", required = false)  String agentUsername) throws SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = missionService.listAllMissionsOrByAgent(saggieRequestContext, agentUsername);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }

    @DeleteMapping("/{missionId}")
    @ApiVersions({"1"})
    @PreAuthorize("hasRole('ROLE_LEADER')")
    @ApiOperation(value = "Delete Mission", notes = "Delete a Mission. Only Leader can delete mission")
    public ResponseEntity<ApiCommonResponseWs> delete(@AuthenticationPrincipal Authentication principal,
                                                      @PathVariable(name = "missionId") Integer missionId) throws SaggieApiException {
        SaggieRequestContext saggieRequestContext = ApiRequestUtils.getSaggieRequestContext(principal);
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);

        ApiCommonResponseWs apiCommonResponseWs = missionService.delete(saggieRequestContext, missionId);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, httpServletRequest);
        return new ResponseEntity<>(apiCommonResponseWs, HttpStatus.OK);
    }

}
