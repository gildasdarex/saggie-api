/**
 * Created by darextossa on 5/3/17.
 */

package com.saggie.api.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/home")
    public String index() {
        return "index";
    }
}
