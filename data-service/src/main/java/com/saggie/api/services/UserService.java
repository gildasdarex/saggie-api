package com.saggie.api.services;


import com.saggie.api.models.SaggieRoleWs;
import com.saggie.api.models.UserWs;
import com.saggie.api.utils.ApiResponseBuilder;
import com.saggie.commons.ws.enums.*;
import com.saggie.commons.ws.exceptions.SaggieApiException;
import com.saggie.commons.ws.logger.SaggieLogger;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.models.SaggieRequestContext;
import com.saggie.commons.ws.utils.SaggieDbAssert;
import com.saggie.commons.ws.utils.SaggieObjectUtils;
import com.saggie.data.access.entities.RoleEntity;
import com.saggie.data.access.entities.UserEntity;
import com.saggie.data.access.repositories.RoleRepository;
import com.saggie.data.access.repositories.UserRepository;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class UserService {
    private static Logger logger = LogManager.getLogger(UserService.class);

    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    ApiResponseBuilder apiResponseBuilder;
    @Autowired
    private Mapper mapper;

    @Transactional
    public ApiCommonResponseWs create(SaggieRequestContext saggieRequestContext, UserWs userWs) throws SaggieApiException {
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "create");


        // Check if role specified in userWs exists- if not throw an exception
        RoleEntity roleEntity = roleRepository.findByDescription(userWs.getRoleName().toString().toLowerCase());
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_USER, roleEntity, "No role found with desc = " + roleEntity.getDescription());

        // Convert userWs object to UserEntity object and save to database
        UserEntity userEntity = mapper.map(userWs, UserEntity.class);
        userEntity.setRoleId(roleEntity.getId());
        userEntity.setUsername(userWs.getUsername());
        userEntity.setEmail(userWs.getUsername());
        userEntity.setCreatedAt(Timestamp.valueOf(LocalDateTime.now()));
        if(userWs.getRoleName().toString().toLowerCase().equals(SaggieRoleEnums.AGENT.getName().toLowerCase()))
            userEntity.setStatus(SaggieAgentStatusEnums.ALIVE.getUidValue());
        userEntity = userRepository.save(userEntity);



        userWs.setId(userEntity.getId());
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "create");
        return apiResponseBuilder.createApiCommonResponseWs(userWs, ApiContext.CREATE_OR_UPDATE_USER, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }

    @Transactional
    public ApiCommonResponseWs update(SaggieRequestContext saggieRequestContext, UserWs userWs, Integer id) throws SaggieApiException {
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "update");

        // Check if role specified in userWs exists- if not throw an exception
        RoleEntity roleEntity = roleRepository.findByDescription(userWs.getRoleName().toString().toLowerCase());
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_USER, roleEntity, "No role found with name = " + roleEntity.getDescription());

        UserEntity userEntity = userRepository.findById(id);
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_USER, userEntity, "No user found");
        // Convert userWs object to UserEntity object and save to database
        if (SaggieObjectUtils.isNotNull(userWs.getRoleName())){
            roleEntity = roleRepository.findByDescription(userWs.getRoleName().toString().toLowerCase());
            userEntity.setRoleId(roleEntity.getId());
        }

        if (!userWs.getUserStatus().isEmpty()) userEntity.setStatus(SaggieAgentStatusEnums.valueOf(userWs.getUserStatus().toUpperCase()).getUidValue());
        if (!userWs.getFirstName().isEmpty()) userEntity.setFirstName(userWs.getFirstName());
        if (!userWs.getLastName().isEmpty()) userEntity.setLastName(userWs.getLastName());
        userEntity.setUpdatedAt(Timestamp.valueOf(LocalDateTime.now()));
        userEntity = userRepository.save(userEntity);


        // Generate api response
        userWs = mapper.map(userEntity, UserWs.class);
        userWs.setUserStatus(SaggieAgentStatusEnums.fromIntValue(userEntity.getStatus()).toString());
        userWs.setRoleName(SaggieRoleEnums.fromIntValue(userEntity.getRoleId()));

        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "update");
        return apiResponseBuilder.createApiCommonResponseWs(userWs, ApiContext.CREATE_OR_UPDATE_USER, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }

    public ApiCommonResponseWs list(SaggieRequestContext saggieRequestContext) {
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "list");
        UserEntity currentUser = userRepository.findById(saggieRequestContext.getUserId());
        Integer roleId = currentUser.getRoleId();
        List<UserEntity> userEntities = new ArrayList<>();

        //  two roles are allow to access to this api; ADMIN AND LEADER role
        //  User with role ADMIN can see LEADER or AGENT - User with role LEADER can see the Agents
        if (roleId == SaggieRoleEnums.ADMIN.getUidValue()){
            userEntities = userRepository.findByRoleIdIn(Arrays.asList(SaggieRoleEnums.LEADER.getUidValue(),SaggieRoleEnums.AGENT.getUidValue()));
        }
        else {
            userEntities = userRepository.findByRoleIdIn(Arrays.asList(SaggieRoleEnums.AGENT.getUidValue()));
        }

        List<UserWs> userWsList = new ArrayList<>();
        userEntities.stream().forEach(userEntity -> {
            UserWs userWs = mapper.map(userEntity, UserWs.class);
            SaggieRoleWs saggieRoleWs = mapper.map(userEntity.getRoleByRoleId(), SaggieRoleWs.class);
            userWs.setRoleName(SaggieRoleEnums.valueOf(saggieRoleWs.getDescription().toUpperCase()));
            userWsList.add(userWs);
        });

        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "list");
        return apiResponseBuilder.createApiCommonResponseWs(userWsList, ApiContext.CREATE_OR_UPDATE_USER, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }


    public ApiCommonResponseWs getById(SaggieRequestContext saggieRequestContext, Integer userId) throws SaggieApiException {
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "getById");
        UserEntity currentUser = userRepository.findById(saggieRequestContext.getUserId());
        Integer currentUserRoleId = currentUser.getRoleId();

        UserEntity selectedUser = userRepository.findOne(userId);
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_USER, selectedUser, "No user found with id = " + userId);

        Integer selectedUserRoleId = selectedUser.getRoleId();

        //  two roles are allow to access to this api; ADMIN AND LEADER role
        //  User with role ADMIN can see LEADER or AGENT - User with role LEADER can see the Agents
        if ( currentUserRoleId == SaggieRoleEnums.LEADER.getUidValue() && ( selectedUserRoleId == SaggieRoleEnums.ADMIN.getUidValue() || selectedUserRoleId == SaggieRoleEnums.LEADER.getUidValue() ) ){
            throw  new SaggieApiException(ApiContext.GET_USER, ApiResponseErrorCategory.AUTHORIZATION, " You can not access to this user informations", new Object());
        }

        UserWs userWs = mapper.map(selectedUser, UserWs.class);
        SaggieRoleWs saggieRoleWs = mapper.map(selectedUser.getRoleByRoleId(), SaggieRoleWs.class);
        userWs.setRoleName(SaggieRoleEnums.valueOf(saggieRoleWs.getDescription().toUpperCase()));

        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "getById");
        return apiResponseBuilder.createApiCommonResponseWs(userWs, ApiContext.GET_USER, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }


    @Transactional
    public ApiCommonResponseWs delete(SaggieRequestContext saggieRequestContext, Integer userId) throws SaggieApiException {
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "delete");

        UserEntity userEntity = userRepository.findById(userId);
        SaggieDbAssert.isNull(ApiContext.DELETE_USER, userEntity, "No user found with id = " + userId);

        UserWs userWs = mapper.map(userEntity, UserWs.class);

        userRepository.delete(userId);

        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "getById");
        return apiResponseBuilder.createApiCommonResponseWs(userWs, ApiContext.DELETE_USER, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }

}
