package com.saggie.api.services;


import com.google.common.base.Strings;
import com.saggie.api.models.MissionWs;
import com.saggie.api.models.SaggieMissionStatusWs;
import com.saggie.api.utils.ApiResponseBuilder;
import com.saggie.commons.ws.enums.*;
import com.saggie.commons.ws.exceptions.SaggieApiException;
import com.saggie.commons.ws.logger.SaggieLogger;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.models.SaggieRequestContext;
import com.saggie.commons.ws.utils.SaggieDbAssert;
import com.saggie.commons.ws.utils.SaggieObjectUtils;
import com.saggie.commons.ws.utils.TimeUtils;
import com.saggie.data.access.entities.*;
import com.saggie.data.access.repositories.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

@Component
public class MissionService {
    private static Logger logger = LogManager.getLogger(MissionService.class);

    @Autowired
    private MissionRepository missionRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMissionRepository userMissionRepository;
    @Autowired
    private AgentStatusRepository agentStatusRepository;
    @Autowired
    private ApiResponseBuilder apiResponseBuilder;
    @Autowired
    private MissionStatusRepository missionStatusRepository;
    @Autowired
    private Mapper mapper;

    @Transactional
    public ApiCommonResponseWs create(SaggieRequestContext saggieRequestContext, MissionWs missionWs) throws ParseException, SaggieApiException {
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "create");

        // Check if agent specified for this mission exists- if not throw an exception
        UserEntity userEntity = userRepository.findByUsernameAndStatus(missionWs.getAgentUsername(), SaggieAgentStatusEnums.ALIVE.getUidValue());
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_MISSION, userEntity, "No user found with username = " + missionWs.getAgentUsername() + " . Agent don't exists or dead or not found since his last mission .");

        MissionStatusEntity missionStatusEntity = missionStatusRepository.findOne(SaggieMissionStatusEnums.NEW.getUidValue());

        // Create the mission
        MissionEntity missionEntity =  mapper.map(missionWs, MissionEntity.class);
        missionEntity.setStartDate(missionWs.getStartDateAsTimestampType());
        missionEntity.setEndDate(missionWs.getEndDateAsTimestampType());
        missionEntity = missionRepository.save(missionEntity);

        // Link the mission to the user
        UserMissionEntity userMissionEntity = new UserMissionEntity();
        userMissionEntity.setMissionId(missionEntity.getId());
        userMissionEntity.setUserId(userEntity.getId());
        userMissionEntity.setStatusId(SaggieMissionStatusEnums.NEW.getUidValue());
        userMissionRepository.save(userMissionEntity);


        missionWs.setId(missionEntity.getId());
        missionWs.setStatus(SaggieMissionStatusEnums.NEW);
        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "create");
        return apiResponseBuilder.createApiCommonResponseWs(missionWs, ApiContext.CREATE_OR_UPDATE_MISSION, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }


    @Transactional
    public ApiCommonResponseWs update(SaggieRequestContext saggieRequestContext, MissionWs missionWs) throws ParseException, SaggieApiException {
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "create");

        // Check if  this mission exists- if not throw an exception
        MissionEntity missionEntity = missionRepository.findById(missionWs.getId());
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_MISSION, missionEntity, "No mission found with id = " + missionWs.getId());

        UserMissionEntity userMissionEntity = userMissionRepository.findOneByMissionId(missionWs.getId());
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_MISSION, missionEntity, "No mission found with id = " + missionWs.getId());


        UserEntity userEntity = null;
        // Check if agent specified for this mission exists- if not throw an exception
        if(!Strings.isNullOrEmpty(missionWs.getAgentUsername())){
            userEntity = userRepository.findByUsernameAndStatus(missionWs.getAgentUsername(), SaggieAgentStatusEnums.ALIVE.getUidValue());
            SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_USER, userEntity, "No user found with username = " + missionWs.getAgentUsername() + " . Agent don't exists or dead or not found since his last mission .");
        }


        if(SaggieObjectUtils.isNotNull(userEntity)){
            userMissionEntity.setUserId(userEntity.getId());
        }

        updateMissionStatus( missionWs,  userMissionEntity);

        userMissionEntity = userMissionRepository.findOneByMissionId(missionWs.getId());
        missionWs = getMissionWsFromEntity(userMissionEntity);

        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "update");
        return apiResponseBuilder.createApiCommonResponseWs(missionWs, ApiContext.CREATE_OR_UPDATE_MISSION, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }

    public ApiCommonResponseWs listAllMissionsOrByAgent(SaggieRequestContext saggieRequestContext, String agentUsername) throws SaggieApiException{
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "create");

        // Check if agent specified for this mission exists- if not throw an exception
        if(!Strings.isNullOrEmpty(agentUsername)){
            UserEntity userEntity = userRepository.findByUsername(agentUsername);
            SaggieDbAssert.isNull(ApiContext.LIST_MISSIONS, userEntity, "No user found with username = " + agentUsername );
        }

        List<UserMissionEntity> userMissionEntities = Strings.isNullOrEmpty(agentUsername)? userMissionRepository.findAll(): userMissionRepository.findByUserByUserId_Username(agentUsername);

        List<MissionWs> missionWsList = new ArrayList<>();

        userMissionEntities.stream().forEach(userMissionEntity -> {
            MissionWs missionWs = getMissionWsFromEntity(userMissionEntity);
            missionWs.setAgentUsername(agentUsername);

            missionWsList.add(missionWs);
        });

        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "listMissionsByAgent");
        return apiResponseBuilder.createApiCommonResponseWs(missionWsList, ApiContext.LIST_MISSIONS, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);
    }

    @Transactional
    public ApiCommonResponseWs delete(SaggieRequestContext saggieRequestContext, Integer missionId) throws  SaggieApiException{
        SaggieLogger.logRequestIn(logger, Level.DEBUG, saggieRequestContext, "delete");

        // Check if agent specified for this mission exists- if not throw an exception
        MissionEntity missionEntity = missionRepository.findById(missionId);
        SaggieDbAssert.isNull(ApiContext.DELETE_MISSION, missionEntity, "No mission found with id = " + missionId );

        UserMissionEntity userMissionEntity = userMissionRepository.findOneByMissionId(missionId);
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_MISSION, missionEntity, "No mission found with id = " + missionId);

        userMissionRepository.delete(userMissionEntity);
        missionRepository.delete(missionId);


        SaggieLogger.logRequestOut(logger, Level.DEBUG, saggieRequestContext, "delete");
        return apiResponseBuilder.createApiCommonResponseWs(missionId, ApiContext.DELETE_MISSION, ApiResponseErrorCategory.NONE, ApiResponseStatus.SUCCESS);

    }


    private MissionWs getMissionWsFromEntity(UserMissionEntity userMissionEntity) {
        MissionWs missionWs = new MissionWs();
        missionWs.setId(userMissionEntity.getMissionId());
        missionWs.setName(userMissionEntity.getMissionByMissionId().getName());
        missionWs.setStatus(SaggieMissionStatusEnums.fromIntValue(userMissionEntity.getStatusId()));
        missionWs.setStartAt(TimeUtils.longToDate(userMissionEntity.getMissionByMissionId().getStartDate().getTime()));
        missionWs.setEndAt(TimeUtils.longToDate(userMissionEntity.getMissionByMissionId().getEndDate().getTime()));

        return missionWs;
    }


    private void updateMissionStatus(MissionWs missionWs, UserMissionEntity userMissionEntity) throws SaggieApiException{
        if(SaggieObjectUtils.isNull(missionWs.getStatus()))
            return ;

        // Check if agent specified for this mission exists- if not throw an exception
        AgentStatusEntity agentStatusEntity = agentStatusRepository.findOne(missionWs.getStatus().getUidValue());
        SaggieDbAssert.isNull(ApiContext.CREATE_OR_UPDATE_MISSION, agentStatusEntity, "No Status found with id  = " + missionWs.getStatus().getUidValue() );

        userMissionEntity.setStatusId(missionWs.getStatus().getUidValue());
        userMissionRepository.save(userMissionEntity);

        UserEntity agent = userMissionEntity.getUserByUserId();

        if(missionWs.getStatus().getUidValue() == SaggieMissionStatusEnums.AGENT_DEAD.getUidValue()){
            agent.setStatus(SaggieAgentStatusEnums.DEAD.getUidValue());
        }
        else if (missionWs.getStatus().getUidValue() == SaggieMissionStatusEnums.AGENT_NOT_FOUND.getUidValue()){
            agent.setStatus(SaggieAgentStatusEnums.MISSING.getUidValue());
        }

        userRepository.save(agent);
    }

    private void updateMissionEntity(MissionWs missionWs, MissionEntity missionEntity) throws ParseException{
        if(!Strings.isNullOrEmpty(missionWs.getName())){
            missionEntity.setName(missionWs.getName());
        }

        if(!Strings.isNullOrEmpty(missionWs.getEndAt())){
            missionEntity.setEndDate(missionWs.getEndDateAsTimestampType());
        }

        if(!Strings.isNullOrEmpty(missionWs.getStartAt())){
            missionEntity.setEndDate(missionWs.getStartDateAsTimestampType());
        }

        missionRepository.save(missionEntity);
    }
}
