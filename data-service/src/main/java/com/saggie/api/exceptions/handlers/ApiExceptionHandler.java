/**
 * Copyright (c) 2019, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilalshah on 2019-09-01
 */
package com.saggie.api.exceptions.handlers;

import com.google.common.base.Strings;
import com.saggie.api.utils.ApiResponseInfoManager;
import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;
import com.saggie.commons.ws.enums.ApiResponseStatus;
import com.saggie.commons.ws.exceptions.SaggieApiException;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.models.ApiResponseInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
    private static Logger logger = LogManager.getLogger(ApiExceptionHandler.class);


    @Autowired
    ApiResponseInfoManager apiResponseInfoManager;
    /**
     * Handle MissingServletRequestParameterException. Triggered when a 'required' request parameter is missing.
     *
     * @param ex      MissingServletRequestParameterException
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";
        return buildResponseEntity(new SaggieApiException(ApiContext.NONE, ApiResponseErrorCategory.NONE, error, status.toString()), HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(value = {
            SaggieApiException.class
    })
    public ResponseEntity<ApiCommonResponseWs> handleException(SaggieApiException saggieApiException, HttpServletRequest request) {
        ApiContext apiResponseContext = saggieApiException.getApiContext()==null? saggieApiException.getApiContext():ApiContext.NONE;
        ApiResponseErrorCategory apiResponseErrorCategory = saggieApiException.getApiResponseErrorCategory()==null? saggieApiException.getApiResponseErrorCategory():ApiResponseErrorCategory.NONE;

        ApiResponseInfo apiResponseInfo = apiResponseInfoManager.getApiResponseInfo(apiResponseContext, apiResponseErrorCategory);

        ApiCommonResponseWs apiCommonResponseWs = new ApiCommonResponseWs();
        apiCommonResponseWs.setApiResponseCode(apiResponseInfo.getApiResponseCode());
        apiCommonResponseWs.setApiContext(apiResponseContext);
        apiCommonResponseWs.setApiResponseErrorCategory(apiResponseErrorCategory);
        apiCommonResponseWs.setStatus(ApiResponseStatus.FAILURE);
        apiCommonResponseWs.setData(saggieApiException.getData());

        if(!Strings.isNullOrEmpty(saggieApiException.getMessage())){
            apiResponseInfo.setApiResponseMessage(saggieApiException.getMessage());
            apiCommonResponseWs.setApiResponseMessage(apiResponseInfo.getApiResponseMessage());
            return new ResponseEntity<>(apiCommonResponseWs,  HttpStatus.INTERNAL_SERVER_ERROR);

        }


        if(Strings.isNullOrEmpty(apiResponseInfo.getApiResponseMessage())){
            StringWriter errors = new StringWriter();
            saggieApiException.printStackTrace(new PrintWriter(errors));
            apiResponseInfo.setApiResponseMessage(errors.toString());
            apiCommonResponseWs.setApiResponseMessage(apiResponseInfo.getApiResponseMessage());

        }
        return new ResponseEntity<>(apiCommonResponseWs,  HttpStatus.INTERNAL_SERVER_ERROR);


    }


    /**
     * Handle MethodArgumentNotValidException. Triggered when an object fails @Valid validation.
     *
     * @param ex      the MethodArgumentNotValidException that is thrown when @Valid validation fails
     * @param headers HttpHeaders
     * @param status  HttpStatus
     * @param request WebRequest
     * @return the ApiError object
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException ex,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {
        SaggieApiException apiError = new SaggieApiException(ApiContext.VALIDATION, ApiResponseErrorCategory.INVALID_DATA, "Validation error");
        Map<String, Object> errors = new HashMap<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()){
            errors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }

        for (ObjectError objectError : ex.getBindingResult().getGlobalErrors()){
            errors.put(objectError.getObjectName(), objectError.getDefaultMessage());
        }
        apiError.setData(errors);
        return buildResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles javax.validation.ConstraintViolationException. Thrown when @Validated
    fails.
     *
     * @param ex the ConstraintViolationException
     * @return the ApiError object
     */

    @ExceptionHandler(javax.validation.ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolation(
            javax.validation.ConstraintViolationException ex) {
        SaggieApiException apiError = new SaggieApiException(ApiContext.VALIDATION, ApiResponseErrorCategory.INVALID_DATA, "Validation error");
        apiError.setMessage("Validation error");
        Map<String, Object> errors = new HashMap<>();
        for (ConstraintViolation objectError : ex.getConstraintViolations()){
            errors.put(((PathImpl) objectError.getPropertyPath()).getLeafNode().asString(), objectError.getInvalidValue());
        }
        apiError.setData(errors);
        return buildResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }


    /**
     * Handle DataIntegrityViolationException, inspects the cause for different DB causes.
     *
     * @param ex the DataIntegrityViolationException
     * @return the ApiError object
     */
    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleDataIntegrityViolation(DataIntegrityViolationException ex,
                                                                  WebRequest request) {
        if (ex.getCause() instanceof ConstraintViolationException) {
            return buildResponseEntity(new SaggieApiException(ApiContext.NONE, ApiResponseErrorCategory.DATA_NOT_FOUND, "Database error", HttpStatus.CONFLICT.toString()), HttpStatus.CONFLICT);
        }
        return buildResponseEntity(new SaggieApiException(ApiContext.NONE, ApiResponseErrorCategory.DATA_NOT_FOUND, ex.getMessage(), HttpStatus.CONFLICT.toString()), HttpStatus.CONFLICT);
    }

    /**
     * Handle Exception, handle generic Exception.class
     *
     * @param ex the Exception
     * @return the ApiError object
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    protected ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex,
                                                                      WebRequest request) {
        SaggieApiException apiError = new SaggieApiException(ApiContext.DATA_INTEGRITY, ApiResponseErrorCategory.NONE, String.format("The parameter '%s' of value '%s' could not be converted to type '%s'", ex.getName(), ex.getValue(), ex.getRequiredType().getSimpleName()),
                HttpStatus.BAD_REQUEST.toString());
        return buildResponseEntity(apiError, HttpStatus.BAD_REQUEST);
    }


    private ResponseEntity<Object> buildResponseEntity(SaggieApiException apiError, HttpStatus status) {
        return new ResponseEntity<>(apiError, status);
    }
}
