package com.saggie.api.utils;


import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;
import com.saggie.commons.ws.enums.ApiResponseStatus;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.models.ApiResponseInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ApiResponseBuilder {
    private static Logger logger = LogManager.getLogger(ApiResponseBuilder.class);


    @Autowired
    ApiResponseInfoManager apiResponseInfoManager;
    @Autowired
    private Mapper mapper;

    public ApiCommonResponseWs createApiCommonResponseWs(Object data, ApiContext apiContext, ApiResponseErrorCategory apiResponseErrorCategory, ApiResponseStatus apiResponseStatus){
        ApiResponseInfo apiResponseInfo = apiResponseInfoManager.getApiResponseInfo(apiContext, apiResponseErrorCategory);
        ApiCommonResponseWs apiCommonResponseWs = new ApiCommonResponseWs();
        apiCommonResponseWs.setData(data);
        apiCommonResponseWs.setApiContext(apiContext);
        apiCommonResponseWs.setApiResponseErrorCategory(apiResponseErrorCategory);
        apiCommonResponseWs.setApiResponseCode(apiResponseInfo.getApiResponseCode());
        apiCommonResponseWs.setApiResponseMessage(apiResponseInfo.getApiResponseMessage());
        apiCommonResponseWs.setStatus(apiResponseStatus);
        return apiCommonResponseWs;
    }

}
