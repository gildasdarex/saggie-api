package com.saggie.api.utils;

import com.google.common.base.Strings;
import com.google.gson.reflect.TypeToken;
import com.saggie.commons.ws.enums.ApiContext;
import com.saggie.commons.ws.enums.ApiResponseErrorCategory;
import com.saggie.commons.ws.models.ApiResponseInfo;
import com.saggie.commons.ws.utils.JSONUtils;
import com.saggie.commons.ws.utils.SaggieObjectUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ResourceUtils;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Configuration
public class ApiResponseInfoManager {
    private static Logger logger = LogManager.getLogger(ApiResponseInfoManager.class);


    @Value("${api.response.info.file.path}")
    private String apiResponseInfoFilePath;

    @Autowired
    ResourceLoader resourceLoader;

    private List<ApiResponseInfo> apiResponseInfos = new ArrayList<>();

    @PostConstruct
    public void init(){
        File apiResponseInfoFile = getApiResponseInfoFile();
        if(apiResponseInfoFile != null){
            Type listType = new TypeToken<List<ApiResponseInfo>>() {}.getType();
            String data = null;
            try {
                data = new String(Files.readAllBytes(Paths.get(apiResponseInfoFile.getAbsolutePath())));
                apiResponseInfos  = JSONUtils.fromJSON(data, listType);
            } catch (IOException e) {
                logger.error("Failed to initialize ApiResponseInfoManager bean");
            }
        }
    }

    private File getApiResponseInfoFile(){
        if(!Strings.isNullOrEmpty(apiResponseInfoFilePath)){
            return new File(apiResponseInfoFilePath);
        }

        try {
            String filePath = resourceLoader.getResource("classpath:configs/api.response.info.json").getFile().getAbsolutePath();
            return new File(filePath);
        } catch (IOException e) {
            logger.error("Failed to get api.response.info.json from  resources");
        }
        return null;
    }

    public ApiResponseInfo getApiResponseInfo(ApiContext apiContext, ApiResponseErrorCategory apiResponseErrorCategory){
        if (SaggieObjectUtils.isNull(apiResponseInfos)) return defaultApiResponseInfo(apiContext, apiResponseErrorCategory);

        try {
            Optional<ApiResponseInfo> optionalApiResponseInfo = apiResponseInfos.stream().
                    filter(item -> item.getApiContext().equals(apiContext) && item.getApiResponseErrorCategory().equals(apiResponseErrorCategory)).
                    findFirst();
            return optionalApiResponseInfo.isPresent() ? optionalApiResponseInfo.get() : defaultApiResponseInfo(apiContext, apiResponseErrorCategory);
        } catch (NullPointerException e) {
            logger.error("Failed to get ApiResponseInfo for " + apiContext.name() + " " + apiResponseErrorCategory.name());
            return defaultApiResponseInfo(apiContext, apiResponseErrorCategory);
        }
    }


    private ApiResponseInfo defaultApiResponseInfo(ApiContext apiContext, ApiResponseErrorCategory apiResponseErrorCategory){
        ApiResponseInfo apiResponseInfo = new ApiResponseInfo();
        apiResponseInfo.setApiContext(apiContext);
        apiResponseInfo.setApiResponseErrorCategory(apiResponseErrorCategory);
        return apiResponseInfo;
    }


}

