/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/23/18
 */
package com.saggie.api.utils;

import com.saggie.commons.ws.models.SaggieRequestContext;
import com.saggie.security.user.SaggieUser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;

public class ApiRequestUtils {
    private static Logger logger = LogManager.getLogger(ApiRequestUtils.class);

    public static SaggieUser getSaggieUser(Authentication authentication){
        SaggieUser saggieUser = (SaggieUser) authentication.getPrincipal();
        return saggieUser;
    }

    public static SaggieRequestContext getSaggieRequestContext(Authentication authentication){
        SaggieUser saggieUser = getSaggieUser(authentication);

        SaggieRequestContext saggieRequestContext =  new SaggieRequestContext.SaggieRequestContextBuilder()
                .addUserId(saggieUser.getUserId())
                .build();
        return saggieRequestContext;
    }
}
