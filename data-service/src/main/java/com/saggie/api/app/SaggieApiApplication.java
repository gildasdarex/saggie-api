/**
 * Copyright (c) 2018, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by darextossa on 9/22/18
 */
package com.saggie.api.app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;
import org.springframework.context.annotation.*;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Locale;

@SpringBootApplication
@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class, HibernateJpaAutoConfiguration.class,
        MongoAutoConfiguration.class, MongoDataAutoConfiguration.class})
@Configuration
@ComponentScan({"com.saggie.api"})
@ImportResource({"classpath:/spring-config.xml", "classpath:/spring-saggie-security.xml"})
@PropertySources({
        @PropertySource(value = "classpath:/saggie-api.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "classpath:/saggie-api-${spring.profiles.active}.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "file:${SPRING_CONFIG_LOCATION}/saggie-api.properties", ignoreResourceNotFound = true),
        @PropertySource(value = "file:${SPRING_CONFIG_LOCATION}/saggie-api-${spring.profiles.active}.properties", ignoreResourceNotFound = true)
})
@EnableAsync
@EnableSwagger2
public class SaggieApiApplication extends SpringBootServletInitializer {

    @Value("${pool.size:1000}")
    private int poolSize;;
    @Value("${queue.capacity:1000}")
    private int queueCapacity;

    public static void main(String[] args) {
        SpringApplication.run(SaggieApiApplication.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(SaggieApiApplication.class);
    }

    @Bean
    public LocaleResolver localeResolver() {
        SessionLocaleResolver slr = new SessionLocaleResolver();
        slr.setDefaultLocale(Locale.ENGLISH);
        return slr;
    }

    @Bean
    public ReloadableResourceBundleMessageSource messageSource() {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:i18n/message");
        messageSource.setCacheSeconds(3600);
        return messageSource;
    }

    @Bean(name="saagieExecutor")
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setMaxPoolSize(poolSize);
        taskExecutor.setQueueCapacity(queueCapacity);
        taskExecutor.afterPropertiesSet();
        return taskExecutor;
    }
}
