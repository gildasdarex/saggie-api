/**
 * Copyright (c) 2016, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilalshah on 20/12/2016
 */
package com.saggie.api.config;

import org.mockito.Mockito;
import org.springframework.boot.actuate.endpoint.HealthEndpoint;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.context.annotation.*;

@Profile("controller-integration-test")
@ComponentScan(basePackages = {"com.saggie.api"})
@ImportResource({"classpath:/db-test-spring-config.xml", "classpath:/spring-saggie-security.xml"})
@EnableAutoConfiguration(exclude={HibernateJpaAutoConfiguration.class})
public class ControllerIntegrationTest {

    @Bean
    public HealthEndpoint healthEndpoint(){
        return Mockito.mock(HealthEndpoint.class);
    }
}
