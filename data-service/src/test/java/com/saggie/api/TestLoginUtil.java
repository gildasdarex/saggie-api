package com.saggie.api;

import com.saggie.security.interfaces.SaggieSecurityConstants;
import com.saggie.security.jwt.JwtUtils;

import javax.servlet.http.Cookie;

public class TestLoginUtil {

    private static final String ADMIN_KNOWN_USER_MAIL = "davids@imf.com";
    private static final String ADMIN_KNOWN_USER_PASSWORD = "Test123$";
    private static final String NON_ADMIN_KNOWN_USER_MAIL = "johnd@imf.com";
    private static final String NON_ADMIN_KNOWN_USER_PASSWORD = "Test123$";
    private static final String AGENT_KNOWN_USER_MAIL = "jbond@imf.com";
    private static final String AGENT_KNOWN_USER_PASSWORD = "Test123$";

    public static Cookie loginWithAdminRole(String username, String password, Integer userId) {
        if (!username.isEmpty() && !password.isEmpty())
            return getLoginCookie(username, userId, new String[]{"ROLE_ADMIN"});

        return getLoginCookie(ADMIN_KNOWN_USER_MAIL, 3, new String[]{"ROLE_ADMIN"} );
    }

    public static Cookie loginWithNonAdminRole() throws Exception {

        return getLoginCookie(NON_ADMIN_KNOWN_USER_MAIL, 2,  new String[]{"ROLE_LEADER"});
    }

    public static Cookie loginWithAdminRole() throws Exception {

        return getLoginCookie(ADMIN_KNOWN_USER_MAIL, 3,  new String[]{"ROLE_ADMIN"});
    }

    public static Cookie loginWithAgentRole() throws Exception {

        return getLoginCookie(AGENT_KNOWN_USER_MAIL, 1,  new String[]{"ROLE_AGENT"});
    }

    private static Cookie getLoginCookie(String username,Integer userId, String[] roles) {
        JwtUtils jwtUtils = new JwtUtils();
        String token = jwtUtils.createTokenForUser(userId, username, "", "", roles);
        return new Cookie(SaggieSecurityConstants.SAGGIE_ACCESS_TOKEN, token);
    }
}
