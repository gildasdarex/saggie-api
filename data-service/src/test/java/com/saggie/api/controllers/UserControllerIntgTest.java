/**
 * Copyright (c) 2019, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilalshah on 2019-09-02
 */
package com.saggie.api.controllers;

import com.google.gson.internal.LinkedTreeMap;
import com.saggie.api.TestLoginUtil;
import com.saggie.api.config.ControllerIntegrationTest;
import com.saggie.api.models.UserWs;
import com.saggie.commons.ws.enums.SaggieRoleEnums;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.utils.JSONUtils;
import org.junit.Assert;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestExecutionListeners(MockitoTestExecutionListener.class)
@ActiveProfiles({"controller-integration-test"})
@ContextConfiguration(classes = ControllerIntegrationTest.class)
@WebMvcTest(value = UserController.class, secure = false, excludeAutoConfiguration = {HibernateJpaAutoConfiguration.class})
public class UserControllerIntgTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    private Cookie ADMIN_ROLE_VALID_TOKEN;
    private Cookie LEADER_ROLE_VALID_TOKEN;
    private Cookie AGENT_ROLE_VALID_TOKEN;

    @BeforeClass
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        LEADER_ROLE_VALID_TOKEN = TestLoginUtil.loginWithNonAdminRole();
        ADMIN_ROLE_VALID_TOKEN = TestLoginUtil.loginWithAdminRole();
        AGENT_ROLE_VALID_TOKEN = TestLoginUtil.loginWithAgentRole();

    }

    @Test
    public void createSpy_whenLeaderLogin_thenUnAuthorizeAccess() throws Exception{
        UserWs userWs = createUser("Iron", "man", SaggieRoleEnums.AGENT, "iron.man@imf.com", "Test123$", "ON_MISSION");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void createSpy_whenAgentLogin_thenUnAuthorizeAccess() throws Exception{
        UserWs userWs = createUser("Iron", "man", SaggieRoleEnums.AGENT, "iron.man@imf.com", "Test123$", "ON_MISSION");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(AGENT_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void createSpy_whenAdminLogin_thenAuthorizeAccess() throws Exception{
        UserWs userWs = createUser("Iron", "man", SaggieRoleEnums.AGENT, "iron.man@imf.com", "Test123$", "ON_MISSION");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andExpect(status().isOk());
        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);
        Assert.assertEquals("iron.man@imf.com", getResultMap(resultActions).get("username"));

        requestBuilder = MockMvcRequestBuilders.delete("/v1/users/" + id)
                .cookie(ADMIN_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);
        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteSpy_whenLeaderLogin_thenUnAuthorizeAccess() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/v1/users/10").cookie(LEADER_ROLE_VALID_TOKEN);
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void deleteSpy_whenAgentLogin_thenUnAuthorizeAccess() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.delete("/v1/users/10").cookie(AGENT_ROLE_VALID_TOKEN);
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void deleteSpy_whenAdminLogin_thenAuthorizeAccess() throws Exception{
        UserWs userWs = createUser("Iron", "man", SaggieRoleEnums.AGENT, "iron.man@imf.com", "Test123$", "ON_MISSION");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andExpect(status().isOk());
        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);
        Assert.assertEquals("iron.man@imf.com", getResultMap(resultActions).get("username"));

        requestBuilder = MockMvcRequestBuilders.delete("/v1/users/" + id)
                .cookie(ADMIN_ROLE_VALID_TOKEN);
        resultActions = mockMvc.perform(requestBuilder);
        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void createSpy_whenAdminLogin_created() throws Exception {
        UserWs userWs = createUser("Iron", "man", SaggieRoleEnums.AGENT, "iron.man@imf.com", "Test123$", "ON_MISSION");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());

        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);
        Assert.assertEquals("iron.man@imf.com", getResultMap(resultActions).get("username"));

        requestBuilder = MockMvcRequestBuilders.delete("/v1/users/" + id)
                .cookie(ADMIN_ROLE_VALID_TOKEN);
        resultActions = mockMvc.perform(requestBuilder);
        resultActions.andDo(print()).andExpect(status().isOk());
    }

    private LinkedTreeMap<String, Object> getResultMap(ResultActions resultActions){
        return ((LinkedTreeMap<String, Object>) JSONUtils.fromJSON(ApiCommonResponseWs.class, resultActions.andReturn().getResponse().getContentAsByteArray()).getData());
    }

    @Test
    public void createLeader_whenEmailEmpty_badRequest() throws Exception {
        UserWs userWs = createUser("John", "Alley", SaggieRoleEnums.LEADER, "", "Test123$", "HOME");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isBadRequest());

        LinkedTreeMap<String, Object> map = getResultMap(resultActions);
        Assert.assertEquals("must not be empty", map.get("username"));

        userWs = createUser("John", "Alley", SaggieRoleEnums.LEADER, "abc", "Test123$", "HOME");
        requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isBadRequest());

        map = getResultMap(resultActions);
        Assert.assertEquals("must be an email", map.get("username"));
    }

    @Test
    public void createLeader_whenAdminLogin_created() throws Exception {
        UserWs userWs = createUser("John", "Alley", SaggieRoleEnums.LEADER, "john.alley@imf.com", "Test123$", "HOME");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());

        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);
        Assert.assertEquals("john.alley@imf.com", getResultMap(resultActions).get("username"));

        requestBuilder = MockMvcRequestBuilders.delete("/v1/users/" + id)
                .cookie(ADMIN_ROLE_VALID_TOKEN);
        resultActions = mockMvc.perform(requestBuilder);
        resultActions.andDo(print()).andExpect(status().isOk());
    }


    @Test
    public void updateLeader_whenAdminLogin_updated() throws Exception {
        UserWs userWs = createUser("John", "Alley", SaggieRoleEnums.LEADER, "john.alley@imf.com", "Test123$", "HOME");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());

        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);
        Assert.assertEquals("leader", getResultMap(resultActions).get("roleName").toString().toLowerCase());

        userWs = createUser("John", "Alley", SaggieRoleEnums.AGENT, "john.alley@imf.com","Test123$", "HOME");
        requestBuilder = MockMvcRequestBuilders.put("/v1/users/" + id)
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());

        id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);
        Assert.assertEquals("agent", getResultMap(resultActions).get("roleName").toString().toLowerCase());

        requestBuilder = MockMvcRequestBuilders.delete("/v1/users/" + id)
                .cookie(ADMIN_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);
        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void getLeader_whenAdminLogin() throws Exception {
        UserWs userWs = createUser("John", "Alley", SaggieRoleEnums.LEADER, "john.alley@imf.com", "Test123$", "HOME");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());

        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);

        requestBuilder = MockMvcRequestBuilders.get("/v1/users").cookie(ADMIN_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());

        requestBuilder = MockMvcRequestBuilders.delete("/v1/users/" + id)
                .cookie(ADMIN_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);
        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void deleteLeader_whenAdminLogin_deleted() throws Exception {
        UserWs userWs = createUser("John", "Alley", SaggieRoleEnums.LEADER, "john.alley@imf.com", "Test123$","HOME");
        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/users")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(userWs));
        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());

        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();
        Assert.assertNotNull(id);
        requestBuilder = MockMvcRequestBuilders.delete("/v1/users/" +id).cookie(ADMIN_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);

        resultActions
                .andDo(print())
                .andExpect(status().isOk());
    }

    private UserWs createUser(String firstName, String lastName, SaggieRoleEnums role, String username, String password, String status) {
        UserWs userWs = new UserWs();
        userWs.setFirstName(firstName);
        userWs.setLastName(lastName);
        userWs.setRoleName(role);
        userWs.setUsername(username);
        userWs.setPassword(password);
        userWs.setUserStatus(status);
        return userWs;
    }
}
