/**
 * Copyright (c) 2019, AskLytics and/or its affiliates. All rights reserved.
 * <p>
 * ASKLYTICS PROPRIETARY/CONFIDENTIAL. Use is subject to Non-Disclosure Agreement.
 * <p>
 * Created by bilalshah on 2019-09-02
 */
package com.saggie.api.controllers;

import com.google.gson.internal.LinkedTreeMap;
import com.saggie.api.TestLoginUtil;
import com.saggie.api.config.ControllerIntegrationTest;
import com.saggie.commons.ws.enums.SaggieMissionStatusEnums;
import com.saggie.commons.ws.enums.SaggieRoleEnums;
import com.saggie.commons.ws.models.ApiCommonResponseWs;
import com.saggie.commons.ws.utils.JSONUtils;
import org.mockito.MockitoAnnotations;
import com.saggie.api.models.MissionWs;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockitoTestExecutionListener;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import javax.servlet.http.Cookie;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestExecutionListeners(MockitoTestExecutionListener.class)
@ActiveProfiles({"controller-integration-test"})
@ContextConfiguration(classes = ControllerIntegrationTest.class)
@WebMvcTest(value = MissionController.class, secure = false, excludeAutoConfiguration = {HibernateJpaAutoConfiguration.class})

// TODO - Implement test when date is invalid
// TODO - Implement test when admin or agent try to create or delete a mission
public class MissionControllerIntgTest extends AbstractTestNGSpringContextTests {
    @Autowired
    private MockMvc mockMvc;
    private Cookie ADMIN_ROLE_VALID_TOKEN;
    private Cookie LEADER_ROLE_VALID_TOKEN;
    private Cookie AGENT_ROLE_VALID_TOKEN;

    @BeforeClass
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        LEADER_ROLE_VALID_TOKEN = TestLoginUtil.loginWithNonAdminRole();
        ADMIN_ROLE_VALID_TOKEN = TestLoginUtil.loginWithAdminRole();
        AGENT_ROLE_VALID_TOKEN = TestLoginUtil.loginWithAgentRole();
    }

    @Test
    public void createMission_whenLeaderLogin_thenAuthorizeAccess() throws Exception{
        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void createMission_whenLeaderLogin_thenAuthorizeAccess_WithWrongAgentUsername() throws Exception{
        MissionWs missionWs = createMissionWs("tdarex@imf.com", "Mission Delta",  "2019-01-11'T'17:34:34.898'Z'",  "2019-01-17'T'17:34:34.898'Z'");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().is5xxServerError());
    }

    @Test
    public void createMission_whenAgentLogin_thenUnAuthorizeAccess() throws Exception{
        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(AGENT_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void createMission_whenAdminLogin_thenUnAuthorizeAccess() throws Exception{
        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(ADMIN_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);
        resultActions.andExpect(status().isForbidden());
    }

    @Test
    public void deleteMission_whenLeaderLogin_thenAuthorizeAccess() throws Exception{
        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta 1",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());
        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();

        requestBuilder = MockMvcRequestBuilders.delete("/v1/missions/" + id).cookie(LEADER_ROLE_VALID_TOKEN);
        resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());

    }

    @Test
    public void deleteMission_whenAgentLogin_thenUnAuthorizeAccess() throws Exception{
        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta 2",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());
        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();

        requestBuilder = MockMvcRequestBuilders.delete("/v1/missions/" + id).cookie(AGENT_ROLE_VALID_TOKEN);
        resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isForbidden());
    }

    @Test
    public void deleteMission_whenAdminLogin_thenUnAuthorizeAccess() throws Exception{
        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta 2",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());
        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();

        requestBuilder = MockMvcRequestBuilders.delete("/v1/missions/" + id).cookie(ADMIN_ROLE_VALID_TOKEN);
        resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isForbidden());
    }

    @Test
    public void UpdateMission_whenLeaderLogin_thenAuthorizeAccess() throws Exception{

        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta 3",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());

        Integer id = ((Double) getResultMap(resultActions).get("id")).intValue();

        MissionWs updatedMissionWs = createMissionWs(null, "Mission Delta 5",  "12-01-2018 11:11:11",  "14-01-2018 11:11:11", SaggieMissionStatusEnums.AGENT_NOT_FOUND);
        updatedMissionWs.setId(id);


        requestBuilder = MockMvcRequestBuilders.put("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(updatedMissionWs));

        resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void listAllMission_whenLeaderLogin_thenAuthorizeAccess() throws Exception{

        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());

        requestBuilder = MockMvcRequestBuilders.get("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void listMissionByForExistingAgent_whenLeaderLogin_thenAuthorizeAccess() throws Exception{

        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());

        requestBuilder = MockMvcRequestBuilders.get("/v1/missions?agent=jbond@imf.com")
                .cookie(LEADER_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());
    }

    @Test
    public void listMissionByForNonExistingAgent_whenLeaderLogin_thenAuthorizeAccess() throws Exception{

        MissionWs missionWs = createMissionWs("jbond@imf.com", "Mission Delta",  "12-01-2018 11:11:11",  "12-08-2018 11:11:11");

        RequestBuilder requestBuilder = MockMvcRequestBuilders.post("/v1/missions")
                .cookie(LEADER_ROLE_VALID_TOKEN)
                .contentType(MediaType.APPLICATION_JSON)
                .content(JSONUtils.toJSONWithoutClassName(missionWs));

        ResultActions resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().isOk());

        requestBuilder = MockMvcRequestBuilders.get("/v1/missions?agent=tdarex@imf.com")
                .cookie(LEADER_ROLE_VALID_TOKEN);

        resultActions = mockMvc.perform(requestBuilder);

        resultActions.andDo(print()).andExpect(status().is5xxServerError());
    }

    private MissionWs createMissionWs(String agentUsername, String name, String startDate, String endDate) {
        MissionWs missionWs = new MissionWs();
        missionWs.setAgentUsername(agentUsername);
        missionWs.setName(name);
        missionWs.setStartAt(startDate);
        missionWs.setEndAt(endDate);
        return missionWs;
    }

    private MissionWs createMissionWs(String agentUsername, String name, String startDate, String endDate, SaggieMissionStatusEnums status) {
        MissionWs missionWs = new MissionWs();
        missionWs.setAgentUsername(agentUsername);
        missionWs.setName(name);
        missionWs.setStartAt(startDate);
        missionWs.setEndAt(endDate);
        missionWs.setStatus(status);
        return missionWs;
    }

    private LinkedTreeMap<String, Object> getResultMap(ResultActions resultActions){
        return ((LinkedTreeMap<String, Object>) JSONUtils.fromJSON(ApiCommonResponseWs.class, resultActions.andReturn().getResponse().getContentAsByteArray()).getData());
    }
}

